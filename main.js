// Task 2.1 Поле

const btn = document.getElementById('btn');
const table = document.getElementById('table');

for (let i = 0; i < 3; i++) {
    table.innerHTML += `
    <tr>
      <th></th>
      <th></th>
      <th></th>
    </tr>
    `
}

btn.addEventListener('mousedown', () => {
    table.classList.add('black');
    // table.classList.toggle('black'); если есть условие, что при нажатии
    // кнопки таблица должна поочередно окрашиваться
})
table.onmousedown = (event) => {
    event.target.classList.add('black');
    // event.target.classList.toggle('black');
}


// Task 2.2 Generator

const sequence = (start = 0, step = 1) => {
    let Start;
    return () => {
        Start = start;
        start += step;
        return Start;
    }
}
var generator = sequence(10, 3);
console.log(generator());
var generator2 = sequence(7, 1);
console.log(generator2());
console.log(generator());
console.log(generator2());